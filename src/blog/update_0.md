19-09-2023
19-09-2023
Update

It's been a while since the last post, hasn't it?

I haven't done much for the blog in the meantime, as i've been working on other projects. I have made one rather significant improvement to the back-end of the site, however. I can write my posts in markdown now, which means I don't have to add HTML to a post after it's written anymore. Instead I can do things like

**Bold** or *italic* text,

## Headers,

!!! notice "Notices,"

> Quotes,

    Code,

[links,](#)

and

| tables | tables | tables |
|-       |-       |-       |
| tables | tables | tables |
| tables | tables | tables |

with ease while writing.

Adding markdown support was surprisingly easy, because there's a Python library for that (of course). It actually took more effort to properly style all of these cool new elements.

Also, since I've gotten some questions about it, I'd like to clarify that all of the posts on this blog are written entirely by yours truly. The most recent post, [The Spawn of Stars](/blog/31-10-2022-The-Spawn-of-Stars.html), was me deliberately mimicking H.P. Lovecraft's style, which may have been the source of the questions.

Finally, I can assure you that I am still writing. So expect some fresh posts in the not-too-distant future, some of them may be about the aforementioned other projects...
