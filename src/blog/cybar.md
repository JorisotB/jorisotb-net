19-12-2023
25-03-2024
Cybar

## 00 - Wander

'Where do you go if going home isn't an option?' The question kept coming back. The answer, in this case, was nowhere. Laine wandered aimlessly through Newisle City. A frigid wind blew, snow turning the night sky to static. Laine shivered in the biting cold and pulled their jacket of too-thin fake leather tighter around their shoulders. The train of thought moved further along its loop. 'Why am I still here?' Sure, this was where the opportunities were, where things happened. Supposedly. So far? Nothing. No signs of anything either. Still, something about this place kept Laine from leaving. Maybe a force-field to keep people in, trap them, keep the city relevant. No. It was something else. In spite of everything, Laine had grown used to life here. Not comfortable---their shelter certainly wasn't---but familiar enough with work and the city's rhythms to make leaving much too steep an expense.

As usual, Laine's feet had carried them into Newisle's old dockside district, across the water on the east side of the central island from which the city gets its name. Many of the large brick buildings were still abandoned, even though housing was scarcer than ever. Amid the disrepair were two types of establishment. The first, seedy bars, old cafes, and rundown convenience stores, looked as though they'd always been there, rooted in the streets' asphalt bones. The second was new, made up of sleek clothing stores and galleries, which seemed to desire a certain urban aesthetic without understanding what that really meant. One of the bars stood out to Laine's meandering mind. They must have walked past it many times before. This time, in the snow and the cold, it was less a part of the city's endless background and more of a beacon, a haven from the wind. It had a wooden facade---imitation no doubt---with small windows so full of stickers and posters that it was impossible to see inside. Above the door, a neon sign flickered its soft red light onto the snow-covered pavement. 'Cybar,' It read.

The first thing Laine noticed was the warmth, which hit them as soon as they opened the door. The second thing was the scent, the place actually smelled of wood. Wood and coffee. It reminded Laine of the restaurant their father had worked at back in their home town. Laine stopped in the doorway, giving the room a curious look. Small lights in every colour were scattered all over, giving the space an ethereal feel. Against the back wall stood two positively ancient arcade machines, vibrant video on the screens. Someone with a metal arm was playing a game on one of them. Next to the machines stood an even older jukebox from which a soothing song sounded, just loud enough to fill the room. On the right, two people sat at a table, engaged in a conversation in a language Laine did not recognise. Along the left wall was a long bar, behind which stood an imposing figure with a pair of tiny horns protruding from the forehead, just below the hairline. The place was by no means crowded, but the full picture was in stark contrast to the outside, where snow dampened all stimuli.

The bartender looked up as Laine approached. 'What'll it be then?' The voice was surprisingly bright.
'Don't care. Long as it's hot.' Laine sat down at the seat nearest the door.
'I've got coffee. Or, if you still want to sleep tonight, tea or hot chocolate.'
'I'll take the coffee, I probably wasn't going to get much sleep anyway.'
The bartender nodded. 'How come?'
'Don't know. Just... doesn't happen. I spend all night thinking.'
'About anything in particular?'
'I really don't want to bother you with this.'
'Oh, it's no bother, it's not like I'm busy, with the weather like it is. I'll listen, if you think it'd help.' The bartender set a steaming cup on the counter.
Laine drank and let out a sigh as they felt the warmth radiate into their chest. After a pause, they said 'It's a lot of little things, things I should be doing, things I want to do, but don't.' Laine began another sentence, but stopped. They gave the bartender a pleading look, though it was unclear whether the plea was for more questions or for the conversation to end.
'I get that,' The bartender said, asking no more.
Laine finished their coffee in silence, paid, thanked the bartender, and left.

Later that week, Laine was walking and thinking. Again. They'd tried to go a different route that day, think of other things, take their mind off work. But when lost in thought the autopilot always follows the most familiar way. Even though the previous week's snowstorm had died down, the cold still bit right through Laine's clothes. Without snowfall, the layer of powder had rapidly turned to mush under the wheels of cars and the feet of crowds. The usual noise, no longer suppressed, had returned to Newisle's streets. Laine couldn't focus on their thoughts. When the neon sign came into view again, they remembered the warmth, the coffee, the bartender's questions. They went in.

The bartender greeted Laine with a gesture. 'Welcome back, coffee again?'
'Please,' Laine said while taking the same seat as last time.
'Name's Morgan by the way,' the bartender said while setting the cup on the bar.
'Laine.'
'So, did you get to do the things you wanted to?'
Laine paused, then said 'Honestly, I don't know what I meant by that. I feel like there *are* things I should be doing, but I don't know what.'
'I've heard that a lot since I moved here, y'know. Might be something in the air, might just be the city, but something here has that effect on folks. Most of em can't leave either, it isn't their fault.'
'I chose to come here, though, I chose to stay. Surely that's on me.' It was not phrased as a question.
'A choice is not always made freely. I don't know why you're here, and I won't ask, but I know there are forces outside the individual.' Morgan's expression changed. Laine detected a hint of anger. 'Individualism is the bread and butter of the system. People have to believe that if someone is successful, their personal decisions were responsible for that success. Vice versa, if someone's unsuccessful, perhaps even homeless, that must be the result of a personal failure. Like some capitalist form of karma. It has to be like that, because if it isn't, you have to accept that circumstances beyond one's control determine one's success. And if that's the case, then the system's unfair. Blaming yourself doesn't change that.'
'I don't see how it would help to blame something else.'
'Maybe it wouldn't, but maybe it could be a start. Think on it. And ask for help if you need it.'
'Right, I will,' Laine said, but they didn't.

The third time Laine stepped into Cybar, something was different. The music, Laine realised, was gone.
'What's with the silence?' They asked as they sat down.
'The jukebox broke yesterday. Coffee again?'
'Please. I could take a look at it, if you want, I used to work for an electrician.'
'If you have the time, I'd appreciate it.'
Laine drank their coffee while Morgan fetched a screwdriver, and went to open up the jukebox. Inside, the machine looked as though it had been repaired many times already. Wires and circuits were clearly installed and reinstalled by different hands at different points in time. After inspecting and putting aside several boards, Laine spotted a capacitor that had recently blown out. Morgan went into the back and returned with a toolbox full of electrical parts and a soldering iron. Laine scrubbed off the dirt and soldered on a new capacitor. Immediately upon plugging the power back in, music filled the room once more.
'Thanks a lot,' Morgan said, 'that coffee's on me, as are the next five.' Once Laine sat back down, the bartender continued 'Y'know, I might be able to use your help some more. What I said last time, about asking for help, that wasn't a baseless offer. My friends and I lend a hand to people whose choices---Morgan made air quotes around the word choices---left them down on their luck, and we do so discreetly, if you catch my drift. My offer to you stands, but we happen to be in need of someone who knows their way around electrical matters. I realise this is a big ask, but if you want, you should stick around till closing and I'll introduce you to the others. They can explain the details better.' Morgan paused, then said 'Things you should be doing, right?'

## 01 - Mission

When the last customer left, Laine was still seated at the bar. They watched silently as Morgan put the chairs away, cleaned the tables, and locked the door. Then, still without a word, they followed Morgan through the door behind the bar, down a flight of stairs, and into a cellar. In the corner stood the same metal-armed person Laine had seen at the arcades several evenings ago.
'Laine, meet Hawk,' said Morgan, 'you've probably seen her around upstairs before.'
Hawk stepped forward and shook Laine's hand, the metal warmer to the touch than expected. Up close, Laine noticed the arm wasn't the only modification. Hawk also had pointed ears, like Spock's, and one of her eyes was replaced by a lens with a thin, luminescent ring round it. Greetings exchanged, the group turned the corner towards the rest of the room, which was packed to the brim with all manner of boxes, tools, and parts. There was just enough room left over for a large couch, which was nestled in between storage racks by the left wall. At the far end of the room, typing away at a laptop almost as densely stickered as the windows above, was another person, with long hair dyed in shades of blue and pink.
'And this is Zoey,' Morgan said, 'she's the one who actually knows how any of this is going to work.'
She held up a hand, palm facing out, to indicate she was busy, so Morgan continued explaining, saying 'This operation of ours can't afford to stop. It's mainly medicine and rent that's on the line, y'see. We can't leave people hanging. Meeting in person isn't a certainty anymore, so we rely on the internet often, which is not ideal to say the least. There's never any real anonymity, and the city can simply cut it whenever they want.'
'It's happened before. During the riots last summer,' Hawk chimed in.
'We need another solution, so we're setting up our own network,' Morgan continued again. 'I don't understand all of it, but the gist is we need someone who can work electronics to set it up. Way you fixed that jukebox, seems to me you might be able to help out.'

At this point, Zoey turned her laptop around to show a complicated diagram. 'This is the plan,' she began, her tone excited, 'Tor's all well and good, but it's still the internet, and it can still be cut, so we're setting up our own net, completely self-contained. It'll have actual end-to-end encryption, no back-doors, and it'll be operational and secure no matter what backwards policy any corporation or government decides is good for us. I just need to set up keys to give people access while you're out there installing these relays across the city. I've modelled the coverage, it should be doable if we put them high up. Only way to kill it will be to kill power to the entire city. The switches run a light-weight Unix OS with my own software---'
Laine listened attentively, but the rest of the explanation was so full of jargon and moved so fast, that it flew over their head. When they tuned back in, Zoey was saying 'I've optimised them as much as I can, their power consumption should be a rounding error in the places we're thinking of putting them.'
'And where's that?' Laine asked.
'Rooftops,' she answered, 'they're usually accessible if you know what you're doing, and Hawk's got us covered there, but no-one ever thinks to check them properly. Tapping into live wires isn't easy though, she fried the last box she tried to install, I assume that's why you're here.'
'It is...' Laine's tone was hesitant. Then, surprising even themself with the declaration, Laine said 'I'm in.'

Two days later, Laine and Hawk met at a compound on the edge of the city, armed with a switch, an antenna, and a toolkit. Both had covered their faces and wore nondescript black clothes, including a new windbreaker Laine got from Morgan, which made the cold more bearable. On the way there, they had noticed many more surveillance camera's and police vehicles than ever before, which gave them a sinking feeling of anxiety in their stomach. Carefully, the pair walked around the perimeter, along a chainlink fence, until they got to a stretch that had collapsed. The area wasn't quiet, no place in Newisle City is, but there was no-one around who would spot them as they crossed the fence and disappeared into the imposing construction. To Laine, the place looked almost organic, like an industrial ecosystem of concrete and rust. Tall chimneys sprouted from the ground like trees without branches, pipes snaked along the floors and walls like vines. The wreck of a burnt out car seemed to Laine like a carcass left to rot. The hum of the city could almost be mistaken for the song of digital birds. It was difficult to imagine humans had ever worked in this facility, let alone that it had been built by human hands.

Hawk had clearly scouted the place before, because she walked resolutely towards a stack of crates and began climbing. Laine followed clumsily. After the crates, Hawk took a route across narrow scaffolding, through several broken windows, and eventually up a rusted ladder that led to the roof.
'You never told me what sort of building this is,' Laine said between heavy breaths.
'Used to be an assembly plant.'
'What did they assemble?'
'Implants.'
'What kind?'
'How should I know?'
'It's just, you have a lot of em.'
'Mine aren't from a factory.' With that, Hawk walked off to find a spot for the relay.
The roof was flat, but the pipes and wires running to and from several massive AC units made it hard to navigate. Nevertheless, Hawk quickly spotted an electrical box that was still powered and called Laine over. They got to work while Hawk paced back and forth, keeping watch. Having put on a pair of protective rubber gloves, Laine pried the box open, exposing a labyrinthine mass of wires that made the roof look like an empty parking lot by comparison. They began testing the current on every wire until they managed to isolate one that would provide enough power for both the switch and the antenna. With the wire cut, Laine soldered the switch's power cord onto it and closed the circuit back up. Finally, they affixed the switch to the inside and the antenna to the outside of the box. Laine felt the whole thing went by quickly, but Hawk was clearly getting impatient. The pair hastily went back the way they came.

The following days, Laine and Hawk worked their way down a list of abandoned buildings. After a little more than a week, they moved on to residential buildings with little or no security. Laine's climbing abilities improved steadily and Hawk seemed to get more comfortable with each success. After eight installed relays, the list of easy targets ran out. Next up was a skyscraper just north of the financial district. The building had a shining glass facade, lit with bright light and covered in advertisements. It was part mall, part hotel, and part rentable business spaces. Compared to the measures employed by larger corporations and wealthy business owners, which border on paranoia, the security in this place was mild but very much present.
'Act like you belong,' Hawk said in a hushed voice before they entered the building. 'There will be cameras.'
Inside, they took the elevator as high as it would take them, to the 54th floor. Ten floors still remained. They walked through spotless white hallways with gray carpets and unmarked doors. Some unfortunate janitor must have the sisyphean task of maintaining the sterile state of these corridors, but any sign of their toil had been carefully concealed. Nothing of the exterior's opulent design had made it in here either, leaving only a disorienting liminal space. Just when Laine was about to offer that they were going in circles, they opened a door to the stairwell. The way up was barred by metal grating that that extended over the gap between the stairs, with no obvious way to get the gate open from the outside. Hawk looked around and, without warning, jumped. She grabbed the bars and dangled above certain death for one blood-stilling moment before she swung further and threw one leg over the railing on the other side. She pulled herself up onto the stairs and opened the door to let Laine through. 'That wasn't very inconspicuous. We should hurry,' she said.
When the pair reached the top of the stairs, there was another door in their way. This one had a touchpad embedded in the doorknob. Hawk pulled out a magnet and held it to the pad, which complied with a satisfying click.

Though installing the relay was a routine by now, Laine felt it took too long. Hawk's stunt had surely alerted someone to their presence. Hawk, however, seemed more relaxed than before. 'They haven't come for us yet. The guards either don't care, or didn't see,' she said.
Laine nodded and began walking towards the stairs, but Hawk turned the other direction. 'Let's stay a while. Appreciate the view,' she said. She walked towards the edge of the roof, climbing over a billboard to get there, and sat down with her legs over the edge. Laine hesitated, then carefully joined her. They were facing the south-east, the skyline of the financial district to their right. Its skyscrapers shone brilliantly in the night, each one a luminous monument to the city's wealthiest. A police helicopter hovered in between the two tallest spires, ready to descend upon anyone who would act against them. To the left, across the water, was the city's east side. A blanket of lights extended farther than the fog would let them see, as though the city had no end. Below them, people moved around on all sides like ants. Laine tried to count them, imagining how many more would be out of sight. A silence hung in the air between the two figures on their perilous perch, only broken by the city's ever present hum.
'Moments like this almost make me like living in this city. Almost,' Hawk said after a while. 'The noise and hysteria of the streets are so distant up here.' She looked at the panorama from right to left, savouring it.
Laine followed her gaze. 'You're right. It's beautiful.'
'Doing this, the thrill of it, it makes me appreciate everything more, somehow. I think it keeps me grounded.'
'That's an interesting thing to say, when we're so high up.'
'It is, isn't it. But without this overview, I'd lose track of things. Forget why I keep going. Or something.'
'That makes sense, I think.'
The silence took over once more. They sat there for a while longer, taking the vista in.
'Well. Let's head down,' Hawk said eventually, 'We've only one left, looks like we'll get this done without any trouble.'

The final relay was to be installed on a private building in the very south of the main island. Laine and Hawk got into the building the usual way, simply pretending to belong there, and made it onto the roof. The relay powered up and Laine felt a sense of relief wash over them. Just as they closed the electrical box, Hawk called out 'Fuck. We're spotted. Follow me.'
A small camera drone hovered above them. Hawk grabbed the toolkit and scrambled towards the door with Laine in tow. They ran down a flight of stairs, along a hallway, and into an open elevator. Hawk mashed the button for the second floor, saying 'They'll be guarding the elevators on the ground floor. We'll take the stairs and run past them. Be ready.'
The moment the gap between the elevator doors was wide enough, Hawk bolted out, leapt down the stairs, and dashed into the lobby. As expected, there was security by the elevators, the guards turned and shouted. Hawk was already out of the door, but one of the guards blocked the exit before Laine could follow. Laine ducked under their outstretched arm, stumbled onto the street, and took off after Hawk, who slowed to let them catch up. They sprinted down the street, through crowds of people and across intersections. Two blocks down, they heard sirens blaring behind them. Hawk swore and picked up the pace. As the sirens grew louder, the pair dove into a subway station, pushing past the queue and jumping over the turnstiles. There were no trains, so they ran along the platform and left the station through a different exit. It only took seconds for the sirens to pick up once more, but by then the two were already fleeing down another avenue. Roadworks had pushed the crowd ahead even closer together. Hawk hesitated for a split-second, then scaled the fence into the construction site, stopped to pull Laine up as well, and continued running. At the other end, an open gate let them out into a street market. Laine followed Hawk through the mass of people while a barrage of smells and colours and shouting salespeople assailed their senses. Laine was certain they were going to collapse if not for the adrenaline, but, just then, Hawk slowed down. The sirens were no longer audible. The chase was over.

## 10 - Future

When Laine and Hawk made it back to the bar, they were greeted excitedly by Morgan, who quickly ushered them downstairs.
'You did it! You actually did it!' Zoey exclaimed as soon as they entered the cellar. 'The net is fully functional, I've already sent some messages, I think I could manage audio or even video calls with some more tweaking.'
'Told you I'd get it,' Hawk answered. 'Though Laine certainly helped.'
'I'm fairly certain I installed every single one of those relays,' Laine said as they dropped onto the couch, the fatigue finally catching up to them.
'That you did,' Hawk said, 'Let's call it a team effort.'
'Yes, let's,' Morgan chimed in, 'and let's get something to drink, you two look like you need it.'
Morgan went up to close the bar early and returned to the cellar with all manner of drinks and food. Laine dug in hungrily. Between bites they told Morgan and Zoey about the chase, the severity of the situation already fading.
'They'll be looking for thieves, but we didn't steal anything. We won't be in any more danger than before,' Hawk assured them.
'A toast, then, to our freedom and future,' said Morgan, raising a glass, the others joining in.
The four of them celebrated, talked, laughed, and watched as more and more messages were exchanged on the net. Zoey explained in great detail what new plans she could work out now that there was someone who could do the electronics.
Some time later, Morgan took Laine aside, 'Speaking of the future. Don't you forget that we offered to help you as well now.'
'I... won't.' Hesitation returned to Laine's voice.
'Right.' A knowing smile entered Morgan's face. 'Where are you staying, currently?'
'I have a shelter, it's fine.'
'I'll see if I can pull some strings, it's the least I can do.' Morgan turned back to the room. 'Now, does anyone want another round?'
They toasted once more and went on conversing about everything and nothing until, at around four in the morning, Laine fell fast asleep on the couch.

The following day, as Laine walked back to their shelter, it was snowing again. It would likely the last time this year. The flakes came down lazily, reflecting Newisle's many lights, reminding Laine of that night atop the skyscraper with Hawk. A bit later, Laine noticed a street they had not walked down before and wondered how they could have missed it.

---

## Edit 25-03-2024

I fixed a couple of typos and gave the ending more room to breathe. Thanks to the magic of version control, the story without the edits can still be found on [GitLab](https://gitlab.com/JorisotB/jorisotb-net/-/raw/f1d359402b5be30c63aa441ae3f85a48dbea83dd/src/blog/cybar.md).
